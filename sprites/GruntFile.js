module.exports = function(grunt) {

  // Project configuration.
   grunt.initConfig({
    sprite:{
      all: {
        src: 'img/*.png',
        dest: 'img/spritesheet.png',
        destCss: 'css/sprites.css'
      }
    }
  });
 
  // Load in `grunt-spritesmith` 
  grunt.loadNpmTasks('grunt-spritesmith');
  // Default task(s).
  grunt.registerTask('default', ['sprite']);

};