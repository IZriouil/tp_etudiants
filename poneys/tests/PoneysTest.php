<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    protected $poneys;

    protected function setUp(){

      $this->poneys= new Poneys();

      $this->poneys->setCount(9);

    }

    protected function tearDown()
    { // detruire la variable Poneys
      unset($this->Poneys);
    }
    

    public function test_removePoneyFromField() {
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $Poneys->getCount());
    }

    #TODO
    public function test_addPoneyToField() {
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->addPoneyToField(2);
      
      // Assert
      $this->assertEquals(10, $Poneys->getCount());
    }

    /**
     * @expectedException Exception
    */
    public function testException() {
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->removePoneyFromField(9);      
    }


    /**
     * @dataProvider additionProvider
     */
    public function testRemove($a,$expected)
    {
        $this->assertEquals($expected, 8 - $a);
    }

    public function additionProvider()
    {
        return [
            [3,5 ],
            [7,1 ],
            [2, 6],
            [1, 7]
        ];
    }


    public function testmockGetNames()
    {
        // Create a mock for the Poneys class,
        // only mock the update() method.
        $mock = $this->getMockBuilder(Poneys::class)
                         ->setMethods(['getNames'])
                         ->getMock();

        $mock->expects($this->exactly(1))-> method('getNames')-> willReturn(['Sam','Red']);
        $this->assertEquals(['Sam','Red'],$mock->getNames());
        
    }
    
    public function test_Empty()
    {
        //$Poneys=new Poneys();
        //$Poneys->addPoneyToField(7);
        $val=15-$this->poneys->getCount();
        $this->assertTrue(($val>=0));
    }



  }
 ?>
